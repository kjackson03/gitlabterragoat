provider "aws" {
  region = "us-west-2"
}

resource "aws_security_group" "ssh_traffic" {
  name        = "ssh_traffic"
  description = "Allow SSH inbound traffic"
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    git_commit           = "bf9ca54e13c2cda9805aad94a1b0ee80da751d83"
    git_file             = "terraform/simple_instance/ec2.tf"
    git_last_modified_at = "2021-10-18 17:33:54"
    git_last_modified_by = "kjackson03@gmail.com"
    git_modifiers        = "kjackson03"
    git_org              = "kjackson03"
    git_repo             = "terragoat"
    yor_trace            = "c984d3c0-5ce1-4fef-bb78-f58d9efadbc6"
  }
}

resource "aws_instance" "web_server_instance" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.ssh_traffic.name}"]
  tags = {
    Name                 = "bc_workshop_ec2"
    git_commit           = "bf9ca54e13c2cda9805aad94a1b0ee80da751d83"
    git_file             = "terraform/simple_instance/ec2.tf"
    git_last_modified_at = "2021-10-18 17:33:54"
    git_last_modified_by = "kjackson03@gmail.com"
    git_modifiers        = "kjackson03"
    git_org              = "kjackson03"
    git_repo             = "terragoat"
    yor_trace            = "fc43b174-b36a-4145-a742-93172c5e44b7"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
